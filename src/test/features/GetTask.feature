Feature: Get Task
  Scenario: User calls get all tasks web service
	Given a task exists with a task name of t22
	When a user retrieves web service response
	Then response status code is 200
    And response fields match the following
	  | description | Task 22 |
	  | task_name	| t22	  |