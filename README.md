Overview
========
Demonstration of Rest-Assured and Cucumber for web services automation. 
There are two test applications 
1. [Google books API](https://developers.google.com/books/docs/v1/getting_started)
2. [Flask Services Demo App](https://bitbucket.org/elx_robjahn/flask-app)


Tests
-----
* Tests are in Gerkin feature files within `/src/test/feature` 
* Feature steps are defined within `/src/test/java/stepdefs`
* Test classes that testNG calls are within `/src/test/java/tests`
* testng.xml list the test to run. For example: `<class name="tests.TestGetAllTasks"/>`

How to Run
----------
There is both Maven POM file and Gradle Build file as a demo. From command line:
* `maven verify` - This will output reports to target/ subfolder
* `gradle test` - This will output resports build/testngOutput
   
Reference
---------
* https://medium.com/agile-vision/cucumber-bdd-part-2-creating-a-sample-java-project-with-cucumber-testng-and-maven-127a1053c180
* http://angiejones.tech/rest-assured-with-cucumber-using-bdd-for-web-services-automation
* https://github.com/sahajamit/cucumber-jvm-testng-integration
   
